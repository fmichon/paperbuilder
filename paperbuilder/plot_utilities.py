import matplotlib
import numpy as np

from copy import deepcopy

from paperbuilder.utilities import str_pvalue

class custom_color_context(object):
    def __init__(self, colors=None):
        self._dict_copy = deepcopy(matplotlib.colors._colors_full_map)
        self._colors = colors
    def __enter__(self):
        if not self._colors is None:
            matplotlib.colors._colors_full_map.update(self._colors)
            matplotlib.colors._colors_full_map.cache.clear()
        return matplotlib.colors._colors_full_map
    
    def __exit__(self, exc_type, exc_value, exc_traceback):
        matplotlib.colors._colors_full_map.clear()
        matplotlib.colors._colors_full_map.update(self._dict_copy)
        matplotlib.colors._colors_full_map.cache.clear()

class custom_style_library_context(object):
    def __init__(self, styles=None):
        self._dict_copy = deepcopy(matplotlib.style.core._base_library)
        self._styles = styles
    def __enter__(self):
        if not self._styles is None:
            matplotlib.style.core._base_library.update(self._styles)
        return matplotlib.style.core._base_library
    
    def __exit__(self, exc_type, exc_value, exc_traceback):
        matplotlib.style.core._base_library.clear()
        matplotlib.style.core._base_library.update(self._dict_copy)

def load_style_library(p):
    matplotlib.style.core.USER_LIBRARY_PATHS.append(str(p))
    matplotlib.style.reload_library()

def annotate_significance(ax, x1, x2, y, label='', color='k'):
    ax.plot([x1, x2], [y, y], color=color, marker=matplotlib.markers.TICKDOWN, mew=matplotlib.rcParams['lines.linewidth']*0.7)
    ax.text((x1+x2)/2., y, label, color=color, ha='center', va='bottom')

def format_pvalue(p, kind='stars'):
    p_ref = [0., 0.001, 0.01, 0.05, 1.]

    idx = np.digitize(p, p_ref)
    if idx==0 or p>1.:
        raise ValueError('p-value out of range')

    if kind=='stars':
        if idx>=len(p_ref)-1:
            return "n.s."
        else:
            return "*"*(len(p_ref)-idx-1)
    elif kind=='exact':
        #return "p={:.2f}".format(p)
        return "p={}".format(str_pvalue(p))
    else:
        if idx==len(p_ref)-1:
            return "p>{}".format(p_ref[idx-1])
        else:
            return "p<{}".format(p_ref[idx])

class NBPlotOptions:
    analysis = None
    style_library = None
    named_colors = None

    def __init__(self, path='.', override=False):
        from paperbuilder.paper import PaperAnalysis
        
        # get paper analysis
        if NBPlotOptions.analysis is None or override:
            NBPlotOptions.analysis = PaperAnalysis(path)
            
            # get matplotlib named styles and colors
            NBPlotOptions.style_library = deepcopy(matplotlib.style.core._base_library)
            NBPlotOptions.named_colors = deepcopy(matplotlib.colors._colors_full_map)

    def set_options(self):
        # install named colors, named styles and plot style
        
        if NBPlotOptions.analysis is None:
            return
        
        options = NBPlotOptions.analysis.load_plot_options()

        matplotlib.colors._colors_full_map.update(options.get('colors', {}))
        matplotlib.colors._colors_full_map.cache.clear()
        
        matplotlib.style.core._base_library.update(options.get('style_library', {}))

        matplotlib.style.use(options.get('style', []))
    
    def unset_options(self):
        # revert to cached values
        if not NBPlotOptions.named_colors is None:
            matplotlib.colors._colors_full_map.clear()
            matplotlib.colors._colors_full_map.update(NBPlotOptions.named_colors)
            matplotlib.colors._colors_full_map.cache.clear()
        
        if not NBPlotOptions.style_library is None:
            matplotlib.style.core._base_library.clear()
            matplotlib.style.core._base_library.update(NBPlotOptions.style_library)

        matplotlib.style.use('default')

