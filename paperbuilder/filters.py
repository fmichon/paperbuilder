
from paperbuilder.utilities import str_exp, str_pvalue


def show_stats_group(x, info=True, order=None, units='', kind='meanstd'):
    s = ""

    if info:
        if kind=='meanstd':
            s += "mean\u202F$\pm$\u202Fsd, "
        elif kind=='meanci':
            s += "mean [99% CI], "
    
    if order is None:
        order = list(x.keys())
    
    # HACK
    for k in ['animal_count', 'date_count', 'trial_count', 'units']:
        if k in order:
            order.remove(k)

    for idx,k in enumerate(order):
        s += "{}: ".format(k)
        s += show_stats(x[k], info=False, units=x.get('units',units), kind=kind)
        if idx+1<len(order):
            s += ", "
    
    return s

def show_stats(x, info=True, units='', kind='meanstd', precision=2):
    s = ""
    if kind=='meanstd':
        if info:
            s += "mean\u202F$\pm$\u202Fsd, "
        s += "{:.{decimals}f}\u202F$\pm$\u202F{:.{decimals}f}{}{}".format(x['mean'], x['std'], "\u202F" if units else "", units, decimals=precision)
        #s += "{}\u202F$\pm$\u202F{}{}{}".format(str_exp(x['mean'],precision), str_exp(x['std'],precision), "\u202F" if units else "", units)
    elif kind=='meanci':
        if info:
            s += "mean [99% CI], "
        s += "{:.{decimals}f}{}{} [{:.{decimals}f},{:.{decimals}f}]".format(x['mean'], "\u202F" if units else "", units, x['ci99'][0], x['ci99'][1], decimals=precision)
        #s += "{}{}{} [{},{}]".format(str_exp(x['mean'],precision), "\u202F" if units else "", units, str_exp(x['ci99'][0],precision), str_exp(x['ci99'][1],precision))
    elif kind=='medianiqr':
        if info:
            s += "median [iqr], "
        s += "{:.{decimals}f}{}{} [{:.{decimals}f},{:.{decimals}f}]".format(x['median'], "\u202F" if units else "", units, x['pct25'], x['pct75'], decimals=precision)
        #s += "{}{}{} [{},{}]".format(str_exp(x['median'],precision), "\u202F" if units else "", units, str_exp(x['pct25'],precision), str_exp(x['pct75'],precision))
    elif kind=='mean':
        if info:
            s += "mean, "
        s += "{:.{decimals}f}{}{}".format(x['mean'], "\u202F" if units else "", units, decimals=precision)

    return s

def show_two_sample_test(x, labels=None, info=True):

    if labels is None:
        labels = [0,1]

    s=''

    if x['test'] in ['ttest', 'welch']:
        if x['alternative']=='greater': alt='>'
        elif x['alternative']=='less': alt='<'
        else: alt='='
        
        if x['paired']:
            H0 = '$H_0:\mu_{{{a}}}-\mu_{{{b}}}{alt}0$'.format(a=labels[0], b=labels[1], alt=alt)
        else:
            H0 = '$H_0:\mu_{{{}}}{}\mu_{{{}}}$'.format(labels[0], alt, labels[1])

        s = '{} samples {}, {}, t({})={:.2f}, p={}'.format(
            'paired' if x['paired'] else 'independent',
            't-test' if x['test']=='ttest' else 'Welch test',
            H0, x['df'], x['statistic'], str_pvalue(x['p']))
    
    if x['test']=='wilcoxon':
        s = '{}Z={:.2f}, p={}'.format('Wilcoxon signed-rank test: ' if info else '',
            x['statistic'], str_pvalue(x['p']))
    
    if x['test']=='kruskal':
        s = '{}H={:.2f}, p={}'.format('Kruskal-Wallis test: ' if info else '',
            x['statistic'], str_pvalue(x['p']))
    
    if x['test']=='mann-whitney':
        s = '{}U={:.2f}, p={}'.format('Mann-Whitney test: ' if info else '',
            x['statistic'], str_pvalue(x['p']))

    if x['test']=='mcnemar':
        s = '{}$H_0:p_{{{}}}=p_{{{}}}$, $\chi^2$={:.2f}, p={}'.format(
            'McNemar test, ' if info else '',
            labels[0], labels[1], x['statistic'], str_pvalue(x['p']))
    
    if x['test']=='propz':
        s = '{}z={:.2f}, p={}'.format(
            'two proportion Z-test, ' if info else '',
            x['statistic'], str_pvalue(x['p'])
        )

    elif x['test']=='mean':
        if x['paired']:
            s = 'mean {}-{} difference [{:.0f}% CI]: {:.2f} [{:.2f},{:.2f}]'.format(labels[1], labels[0],
                100-x['alpha']*100, x['mu'], x['ci'][0], x['ci'][1])
        else:
            s = '$\mu_{{{}}}-\mu{{{}}}$ [{:.0f}% CI]: {:.2f} [{:.2f},{:.2f}]'.format(labels[1], labels[0],
                100-x['alpha']*100, x['mu'], x['ci'][0], x['ci'][1])

    elif x['test']=='pearson':
        s = '{}r={:.2f}, n={:d}, p={}'.format(
            'Pearson correlation: ' if info else '',
            x['correlation'], x['n'], str_pvalue(x['p']))

    return s

def show_iqr(x, info=False, fmt='.2f'):
    if info:
        s = 'inter-quartile range: '
    else:
        s = ''
    
    fmt = '{{:{f}}}-{{:{f}}}'.format(f=fmt)
    s += fmt.format(x['pct25'], x['pct75'])

    return s

def show_range(x, info=False, fmt='.2f'):
    if info:
        s = 'range: '
    else:
        s = ''
    
    fmt = '{{:{f}}}-{{:{f}}}'.format(f=fmt)
    s += fmt.format(x['min'], x['max'])

    return s

def show_sessions(x):
    return '{} sessions in {} animals'.format(x['date_count'], x['animal_count'])

def bold(x):
    return '**{}**'.format(x)

def reg_beta(x):
    N = len(x['x'])
    params = [r"$\beta_{}$={} [{},{}]".format(
                n,
                str_exp(x['x'][n], 2),
                str_exp(x['ci99'][n][0], 2),
                str_exp(x['ci99'][n][1], 2)) for n in range(N)]
    s = ", ".join(params)
    return s

def glm_param(x, estimator='mode'):
    return r"{} [{},{}]".format(str_exp(x[estimator], 2),
        str_exp(x['hpd'][0], 2), str_exp(x['hpd'][1], 2))

FILTERS = {
    'fmt': show_stats,
    'gfmt': show_stats_group,
    'test2': show_two_sample_test,
    'int': int,
    'iqr': show_iqr,
    'range': show_range,
    'ncount': show_sessions,
    'pval': str_pvalue,
    'reg': reg_beta,
    'glm': glm_param,
}