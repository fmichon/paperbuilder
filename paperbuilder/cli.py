import argparse
import pkg_resources

from paperbuilder.commands import commands
from paperbuilder.utilities import package_info


# main command line parser
parser = argparse.ArgumentParser(prog='paper', description="Manage and build paper projects.")
# add version
parser.add_argument('--version', action='version', version=package_info()['version'])
# add command subparser
subparsers = parser.add_subparsers(dest='command')
subparsers.required=True

# init [--path PATH] [--version VERSION] [--package PKG] [--description DESCRIPTION] NAME
# create new paper project
cmd = subparsers.add_parser('init', description="Initialize a new paper project.")
cmd.add_argument('path', help='Parent directory of paper project')
cmd.add_argument('-v', '--version', default='draft', help='Name of first paper version')
cmd.add_argument('--package', default=None, help='Python package structure')
cmd.add_argument('-n', '--name', default=None, help='Name of paper project')
cmd.add_argument('-c', '--create', action='store_true', help='Create path if it does not exist')
cmd.add_argument('-f', '--force', action='store_true', help='Force initialization even if path is not empty')
cmd.add_argument('--description', help='Paper project description')
cmd.set_defaults(func=commands['init'])

# create [--path PATH] [--clone CLONE] [--current] NAME
cmd = subparsers.add_parser('create', description="Create new paper version.")
cmd.add_argument('name', help='Name of new paper version')
cmd.add_argument('-p', '--path', default='.', help='Path to paper project')
cmd.add_argument('-c', '--clone', help='Clone existing paper version')
cmd.add_argument('--current', action='store_true', help='Set new paper version as current')
cmd.set_defaults(func=commands['create'])

# remove [--path PATH] NAME
cmd = subparsers.add_parser('remove')
cmd.add_argument('-p', '--path', default='.', help='Path to paper project')
cmd.add_argument('name', help='Name of paper version')
cmd.set_defaults(func=commands['remove'])


# config [--path PATH] [--user | --project | --version VERSION] KEY VALUE
# config [--path PATH] [--user | --project | --version VERSION] --unset KEY
# config [--path PATH] [--user | --project | --version VERSION] --get KEY
# config [--path PATH] [--user | --project | --version VERSION] --list
# config [--path PATH] [--user | --project | --version VERSION] --merged
# config [--path PATH] [--user | --project | --version VERSION] --edit
cmd = subparsers.add_parser('config')
group = cmd.add_mutually_exclusive_group(required=False)
group.add_argument('-p', '--project', dest='target', action='store_const', const=':project:', help='Target paper project configuration file')
group.add_argument('-u', '--user', dest='target', action='store_const', const=':user:', help='Target global user configuration file')
group.add_argument('-v', '--version', dest='target', help='Target paper version configuration file')
group = cmd.add_mutually_exclusive_group(required=False)
group.add_argument('--set', dest='action', action='store_const', const='set', default='set', help='Set config option')
group.add_argument('--unset', dest='action', action='store_const', const='unset', help='Remove config option')
group.add_argument('--get', dest='action', action='store_const', const='get', help='Retrieve config option')
group.add_argument('--list', dest='action', action='store_const', const='list', help='Show config file')
group.add_argument('--merged', dest='action', action='store_const', const='merged', help='Show merged config file')
group.add_argument('--edit', dest='action', action='store_const', const='edit', help='Open config file in editor')
cmd.add_argument('--path', default='.', help='Path to paper project')
cmd.add_argument('key', nargs='?', help='Config option key')
cmd.add_argument('value', nargs='?', default=None, help='Config option value')
cmd.set_defaults(func=commands['config'])

# build [--path PATH] [--version VERSION] [--force] [TARGET [TARGET ...]]
# preprocess and compile paper with pandoc
cmd = subparsers.add_parser('build')
cmd.add_argument('-v', '--version', default=None, help='Paper version')
cmd.add_argument('-f', '--force', action='store_true', help='Force build of paper')
cmd.add_argument('-p', '--path', default='.', help='Path to paper project')
cmd.add_argument('target', nargs='*', help='What paper output files to build')
cmd.set_defaults(func=commands['build'])

# preprocess [--path PATH] [--version VERSION] [--force]
# apply jinja2 template
cmd = subparsers.add_parser('preprocess')
cmd.add_argument('-v', '--version', default=None, help='Paper version')
cmd.add_argument('-f', '--force', action='store_true', help='Force preprocessing of paper')
cmd.add_argument('-p', '--path', default='.', help='Path to paper project')
cmd.set_defaults(func=commands['preprocess'])

# info [--path PATH] [--version VERSION]
# show paper info
cmd = subparsers.add_parser('info')
cmd.add_argument('-v', '--version', default=None, help='Paper version')
cmd.add_argument('-p', '--path', default='.', help='Path to paper project')
cmd.set_defaults(func=commands['info'])

# clean [--path PATH] [--version VERSION] [--all]
# clean build
cmd = subparsers.add_parser('clean')
cmd.add_argument('-v', '--version', default=None, help='Paper version')
cmd.add_argument('--all', dest='complete', action='store_true', help='Clean paper and analyses')
cmd.add_argument('-p', '--path', default='.', help='Path to paper project')
cmd.set_defaults(func=commands['clean'])

# analysis
analysis = subparsers.add_parser('analysis')
subparsers = analysis.add_subparsers(dest='subcommand')
subparsers.required=True

# analysis create NAME [--clone CLONE] [--version VERSION] [--path PATH]
cmd = subparsers.add_parser('create')
cmd.add_argument('name', help='Analysis name')
cmd.add_argument('-v', '--version', default=None, help='Paper version')
cmd.add_argument('-c', '--clone', help='Clone existing analysis')
cmd.add_argument('-p', '--path', default='.', help='Path to paper project')
cmd.set_defaults(func=commands['analysis']['create'])

# analysis remove [NAME [NAME ...]] [--all] [--version VERSION] [--noclean] [--path PATH]
cmd = subparsers.add_parser('remove')
cmd.add_argument('name', nargs='*', help='Analysis name')
cmd.add_argument('--clean', action='store_true', help='Remove analysis outputs')
cmd.add_argument('-v', '--version', default=None, help='Paper version')
cmd.add_argument('--all', dest='complete', action='store_true', help='Remove all analyses')
cmd.add_argument('-p', '--path', default='.', help='Path to paper project')
cmd.set_defaults(func=commands['analysis']['remove'])

# analysis build [NAME [NAME ...]] [--all] [--force] [--version VERSION] [--path PATH]
cmd = subparsers.add_parser('build')
cmd.add_argument('name', nargs='*', help='Analysis name')
cmd.add_argument('-p', '--path', default='.', help='Path to paper project')
cmd.add_argument('-v', '--version', default=None, help='Paper version')
cmd.add_argument('--all', dest='complete', action='store_true', help='Build all analyses')
cmd.add_argument('-f', '--force', action='store_true', help='Force build analysis')
cmd.add_argument('-t', '--target', action='append', help='Analysis targets to build')
cmd.set_defaults(func=commands['analysis']['build'])

# analysis info NAME [--version VERSION] [--path PATH]
# show analysis info
cmd = subparsers.add_parser('info')
cmd.add_argument('name', help='Analysis name')
cmd.add_argument('-v', '--version', default=None, help='Paper version')
cmd.set_defaults(func=commands['analysis']['info'])

# analysis clean [NAME [NAME ...]] [--all] [--version VERSION] [--path PATH]
# clean analysis output
cmd = subparsers.add_parser('clean')
cmd.add_argument('name', nargs='*', help='Analysis name')
cmd.add_argument('-p', '--path', default='.', help='Path to paper project')
cmd.add_argument('-v', '--version', default=None, help='Paper version')
cmd.add_argument('--all', dest='complete', action='store_true', help='Clean all analyses')
cmd.add_argument('-t', '--target', action='append', help='Analysis targets to clean')
cmd.set_defaults(func=commands['analysis']['clean'])


def cli():
    args = parser.parse_args()
    args.func(**vars(args))
    

if __name__ == '__main__':
    cli()
