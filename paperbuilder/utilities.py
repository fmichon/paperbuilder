import sys
import re
from collections import Mapping
import os
import pathlib

def str_exp(p, precision=2):
    if not isinstance(p, str):
        if p<10**(-precision):
            p = r"{:.2g}".format(p)
            c = re.compile(r'(?P<base>[+-]?[0-9.]+)[eE](?P<sign>-?)[+0]*(?P<exp>[0-9]+)')
            out = c.sub(r'$\g<base>{\\times}10^{\g<sign>\g<exp>}$', p)
        else:
            out = "{:.{}g}".format(p, precision)
    return out

def str_pvalue(p):
    return str_exp(p)

def dict_merge(*args, key='', merge=None):
    """Merge dictionaries.

    Parameters
    ----------
    *args : dictionaries
    key : str
        Key (dot notation) for nested dictionaties. Mainly used internally
        for recursive calls to `dict_merge`.
    merge : None or callable
        Custom function to merge two (non dict) values with same key. The function
        should have signature `fcn(left, right, key)`, where left/right are the 
        values of the left/right dictionary in the merge operation. By default,
        the left value is replaced with the right value.

    Returns
    -------
    dict

    """

    if len(args)==0:
        return {}
    
    a = args[0]
    
    for b in args[1:]:    
        for k,v in b.items():
            if k in a and isinstance(v, Mapping) and isinstance(a[k], Mapping):
                dict_merge(a[k], v, key='.'.join([key,k]), merge=merge)
            elif not k in a or merge is None:
                a[k] = v
            else:
                a[k] = merge(a[k], v, key='.'.join([key,k]))
        
    return a

def map_nested_dicts_modify(ob, func):
    """In-place modification of (nested) values in dictionary.

    Parameters
    ----------
    ob : dict
    func : callable
        Funtion to apply to dictionary values.

    """

    for k, v in ob.items():
        if isinstance(v, Mapping):
            map_nested_dicts_modify(v, func)
        else:
            ob[k] = func(v)

# context manager to temporarily set current working directory
class set_cwd():
    def __init__(self, path):
        self._path = path
    
    def __enter__(self):
        self._prev_path = pathlib.Path.cwd()
        os.chdir(self._path)
    
    def __exit__(self, exc_type, exc_value, traceback):
        os.chdir(self._prev_path)

# context manager to temporarily add path to sys.path
class add_path():
    def __init__(self, path):
        self.path = str(path)
        self.added = False

    def __enter__(self):
        if not self.path in sys.path:
            sys.path.insert(0, self.path)
            self.added = True

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            if self.added:
                self.added = False
                sys.path.remove(self.path)
        except ValueError:
            pass

def package_info():
    import pkg_resources

    info = {
        'name': 'paperbuilder',
        'author': 'nerf',
    }

    try:
        info['version'] = pkg_resources.get_distribution(info['name']).version
    except:
        info['version'] = "development"
    
    return info

def get_config_path():
    import appdirs
    
    info = package_info()
    
    path = appdirs.user_config_dir(
            appname=info['name'],
            appauthor=info['author'],
            version=info['version'])
    
    return path
