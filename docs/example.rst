Example Markdown manuscript
===========================

.. code-block:: yaml

    ---
    # YAML header
    # line starting with # are comments

    ---

.. code-block:: md

    <<! This is a comment that will not appear in the final output >>

    For double spacing use:

    \doublespace

    # This is a section header

    ## And this is a subsection header

    Paragraphs are separated by an empty line.

    # Citations

    [@Igloi2015; @Studte2017]
    
    <div id="refs"></div>

    # Figures

    ![figure legend ](figures/figure.png){#fig:behavior}
    
    ![figure legend ](figures/figure_suppl.png){#fig:behavior_suppl tag="S1"}

    figure {@fig:behavior}a

    # Tables

    table {@tbl:table}
    
    ```table
    ---
    caption: 'caption {#tbl:table}'
    alignment: LCCCCC
    width: [0.4, 0.1, 0.2, 0.1, 0.1, 0.1]
    markdown: true
    include: tables/table.csv
    ---
    ```

    # Variables

    <<*variable.value*>>
    <<*variable.value | filter*>>
    <<*variable.value | filter1 | filter2*>>



    